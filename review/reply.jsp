<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.io.*, java.text.*" %>

<html>
<head>
<link rel="stylesheet" href="/bootstrap-4.2.1-dist/css/bootstrap.css">
<script src="/ckeditor/ckeditor.js"></script>

<!-- include libraries(jQuery, bootstrap) -->
<!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet"> -->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>  -->
<!-- <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> --> 

<!-- include summernote css/js -->
<!-- <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet"> -->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script> -->

<!-- 함수 선언 : 선언한 form 의 action 방식 설정 -->
<script language="JavaScript">
	function submitForm(model){	
	
		var title = document.forms[0].title.value;	
				
		var han=/^[가-힣]*$/;
		var eng=/^[a-zA-Z]*$/;
		var pattern_name=/^[가-힣a-zA-Z0-9]{1,10}$/gi;
		var pattern_special=/[~!^{}<>;\/?]/gi;		
		var pattern_blank = /^\s+|\s+$/g;
		
		if (title==null || title==""){
			alert("입력 요구");
			document.forms[0].title.focus();
			return false;
		}
		if(pattern_blank.test(title)==true ){
			alert("앞 뒤 공백 입력 불가");
			document.forms[0].title.focus();
			return false;
		}	
		if (pattern_name.test(title)==false && pattern_special.test(title)==true){
			alert("잘못된 입력");
			document.forms[0].title.value=title;
			document.forms[0].title.focus();
			return false;
		}		
		
		fm.action = "write.jsp?key=REPLY";
		fm.submit();
	}
</script>
<!-- css 스타일 클래스 선언 : input 부분; textArea 부분 -->
<style>
	.inputTitle {
		height: 30px; width: 400px;
	}
	.textArea {
		resize: none; height: 300px; width: 480px; 
	}
</style>
<style>
	td {
		padding-left: 15px;
	}
	
	tr {
		height: 40px;
	}
</style>
<title>이용후기 답글 입력</title>
</head>
<body>
<center>
<h1>이용 후기</h1><br>
<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	String idS=request.getParameter("id");	
//	int id = Integer.parseInt(idS);
	String titleS=request.getParameter("title");
	String rootidS=request.getParameter("rootid");
	int rootid=Integer.parseInt(rootidS);
	String relevelS=request.getParameter("relevel");
	int relevel=Integer.parseInt(relevelS);
	String recntS=request.getParameter("recnt");	
	int recnt=Integer.parseInt(recntS);
	
	// java.text.SimpleDateFormat 클래스 : 지정된 날짜를 문자열로 변환하는 클래스
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd");
	
	// java.util.Date 클래스( Date constructors ) 
	// : Date() -> 생성자 Date 는 현재 날짜와 시간으로 객체를 초기화 (시간은 밀리 초 단위로 측정)
	String today = formatter.format(new java.util.Date());
	
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// Statement 생성 및 연결 설정
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
	
	// ResultSet 선언 및 설정
//	ResultSet rset1 = stmt1.executeQuery("select title,relevel,recnt from gongji_table where id="+idS+";");	
	
	// ResultSet 이 존재하는 동안 반복문 실행
//	while(rset1.next()){
//		titleO=rset1.getString(1);
//		relevel=rset1.getInt(2);
//		recnt=rset1.getInt(3);
//	}
	
	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select ifnull(min(recnt),0) from gongji_table where rootid="+rootid+" and recnt>"+recnt+" and relevel<="+relevel+";");	
	
	// ResultSet 이 존재하는 동안 반복문 실행
	while(rset1.next()){
		recnt=rset1.getInt(1);
	}	
	
	if (recnt==0){
		ResultSet rset2 = stmt2.executeQuery("select ifnull(max(recnt),0)+1 from gongji_table where rootid="+rootid+";");
		while(rset2.next()){
			recnt=rset2.getInt(1);
		}
		rset2.close();
	} else {
		String sql = "update gongji_table set recnt=(recnt+1) where rootid="+rootid+" and recnt>="+recnt+";";
		stmt3.execute(sql);
	}
	
	relevel = relevel+1;
	
	// 답글의 순서를 맞추기 위한 업데이트문 
//	String sql="update gongji_table set recnt=recnt+1 where rootid="+rootidS+" and recnt > "+recnt+";";
//	stmt2.execute(sql);
	
	// 자원 반환
	stmt3.close();
	stmt2.close();	
	rset1.close();
	stmt1.close();
	conn.close();	
%>
<!-- form 선언 : post 방식; name='fm' -->
<form method=post name='fm'>

<div class="container text-center">
<!-- 테이블 출력 -->
<table width=800 cellspacing=1 border=1 class="table table-bordered">
	<tr >
		<td colspan=2><font size=4><b>답글 입력</b></font></td>
	</tr>
	<tr >
		<td width=20% >번호</td>
		<td>답글(INSERT)</td>
	</tr>
	<tr >
		<td >제목</td>
		<td><input type="text" class="form-control" name="title" ></input></td>		
	</tr>
	<tr >
		<td  >일자</td>
		<td><%=today%><input type=hidden name="writeDate" value=<%=today%>></input></td>		
	</tr>
	<tr >
		<td >내용</td>
		<td style="padding-left:0px">
			<textarea id="editor1" name="content" cols=70 rows=300 class="textArea"></textarea>
			<script>
				var cont='';
				//ckeditor라는 오픈소스 에디터를 넣는다.
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                // CKEDITOR.replace( 'content' );
				var editor = CKEDITOR.replace( 'content', {hegiht: 300});

				// The "change" event is fired whenever a change is made in the editor.
				// var that = this;
				editor.on( 'change', function( evt ) {
					// getData() returns CKEditor's HTML content.
					cont = evt.editor.getData();
					console.log( 'Total bytes: ' + evt.editor.getData().length );
				});
            </script>
			<script>
				// $(document).ready(function() {
					// $('#summernote').summernote({
						// tabsize: 2,
						// height: 300,
					// });
				// });
			</script>
		</td>
		</td>		
	</tr>
	<tr >
		<td  >원글</td>
		<td><%=rootidS%>번<input type=hidden name="rootid" value=<%=rootidS%>></input> [<%=titleS%>]</td>		
	</tr>
	<tr >
		<td >댓글수준</td>
		<td><%=relevel%><input type=hidden name="relevel" value=<%=relevel%>></input></td>
	</tr>
	<tr>
		<td>댓글내 순서</td>
		<td><%=recnt%><input type=hidden name="recnt" value=<%=recnt%>></input></td>		
	</tr>
</table>
</div>
<br>

<div class="container text-center">	
<!-- 버튼용 테이블 생성 -->
<table width=800 cellspacing=1 border=0 class="table table-borderless">
	<tr >
		<td width=60%></td>		
		<td width=10% align=left></td>
		<td width=10% align=left></td>
		<td width=10% align=left>
			<!-- 취소 버튼 생성 : OnClick 방식 활용 -->
			<!-- 쓰기 버튼 생성 : OnClick 방식 활용 : submitForm() 함수 호출 -->
			<input type="button" class="btn btn-outline-dark" value="취소" OnClick=location.href='list.jsp'></input>
		</td>
		<td width=10% align=left>
			<input type="button" class="btn btn-outline-dark" value="쓰기" OnClick="submitForm('write')"></input>
		</td>		
	</tr>
</table>
</div>
</form>
</center>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="/bootstrap-4.2.1-dist/js/bootstrap.js"></script>
</body>
</html>