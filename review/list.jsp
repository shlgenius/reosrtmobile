<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>

<%		
	// 선택된 페이지 번호 참조
	String pageNum=request.getParameter("pageNum");
	if(pageNum==null){
		pageNum="1";
	}	
	int currentPage=Integer.parseInt(pageNum);	
	
	// 한페이지에 보일 레코드 숫자
	String pageSizeS=request.getParameter("pageSize");
	if(pageSizeS==null){
		pageSizeS="20";
	}	
	int pageSize=Integer.parseInt(pageSizeS);	
%>

<html>
<head>
<link rel="stylesheet" href="/bootstrap-4.2.1-dist/css/bootstrap.css">

<!-- css 스타일 선언 : a 링크 스타일 -->
<style>	
	a:link { color: #000000; text-decoration: none; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #000000; text-decoration: underline; font-weight: none;}
	a:active { color: #000000; text-decoration: none; font-weight: bold;}
</style>

<style>	
	.inputTitle {
		height: 20px; width: 400px;
	}		
</style>
</head>
<body>
<center>
<h4><b>이용 후기</b></h4><br>
<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// Statement 생성 및 연결 설정
	Statement stmt1 = conn.createStatement();
	
	// 변수 선언 및 초기값 설정
	int totalRecords=0;
	
	// ResultSet 선언 및 설정
	ResultSet rset1=stmt1.executeQuery("select count(*) from gongji_table;");
	
	// ResultSet 이 존재하는 동안 이하 반복문 실행	
	while(rset1.next()){
		totalRecords=rset1.getInt(1);	// 전체 레코드 저장
	}
	// 자원 반환
	rset1.close();
	stmt1.close();	
%>
<%	
	// 총 페이지 수 계산
	int totalPage = 0;							// 총 페이지 변수 설정
	int R = totalRecords % pageSize;			
	if (R == 0){	
		totalPage=totalRecords/pageSize;		// 총 페이지 수 결정
	} else {
		totalPage=totalRecords/pageSize +1;		// 총 페이지 수 결정(나머지가 다를 경우)
	}

	int groupSize = 10;			// 한 그룹의 페이지 개수
	int currentGroup = 0;		// 현재 그룹 설정 초기화


	R = totalPage%groupSize;									// 현재 속한 그룹 설정
	if (R==0){
		currentGroup=currentPage/groupSize;
	} else {
		currentGroup=currentPage/groupSize+1;
	}
	
	currentGroup = (int)Math.floor((currentPage-1)/ groupSize);	// 현재 그룹 
	int groupStartPage = currentGroup*groupSize +1;				// 현 그룹 시작 페이지
	int groupEndPage = groupStartPage+groupSize-1;				// 현 그룹 끝 페이지
	
	if(groupEndPage>totalPage){									// 마지막 페이지 설정
		groupEndPage = totalPage;
	}
	
	int lineCount=0;	
	int fromPT =(currentPage-1)*pageSize+1; //시작 번호
%>

<%	
	// stmt 생성 및 연결 설정
	Statement stmt2 = conn.createStatement();
			
	// ResultSet 선언 및 설정
//	ResultSet rset = stmt.executeQuery("select * from gongji_table order by id desc;");			
	ResultSet rset2 = stmt2.executeQuery("select id, title, date, relevel, viewcnt, active from gongji_table order by rootid desc, recnt asc;");
%>

<!-- 목록 출력용 테이블 생성 -->

<!-- <table class="table" style="table-layout: fixed; white-space: nowrap; word-break:break-all;"> -->
<table class="table" style="table-layout: fixed; white-space: nowrap; font-size: 85%; word-break:break-all;">
	<thead class="thead-dark">
	<tr align=center>
		<th width=20% style="overflow: hidden;">번호</th>
		<th width=35% style="overflow: hidden;">제목</th>
		<th width=30% style="overflow: hidden;">등록일</th>
		<th width=15% style="overflow: hidden;">조회</th>
	</tr>
	</thead>
<%
	
	// java.text.SimpleDateFormat 클래스 : 지정된 날짜를 문자열로 변환하는 클래스
	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd");
	
	// java.util.Date 클래스( Date constructors ) 
	// : Date() -> 생성자 Date 는 현재 날짜와 시간으로 객체를 초기화 (시간은 밀리 초 단위로 측정)
	String today = formatter.format(new java.util.Date());
	
	String showNew = "";	// 새로운 글 표시
	String showBar = "";	// 답글 표시

	// ResultSet 이 존재하는 동안 이하 반복문 실행	
	while(rset2.next()){
		int id = rset2.getInt(1);
		String title = rset2.getString(2);
		String writeDate = rset2.getString(3);
		int relevel = rset2.getInt(4);
		int viewcnt = rset2.getInt(5);
		int active =rset2.getInt(6);
		
		// 변수 증가
		lineCount++;
		
		// 변수값과 비교하여 break 시기 결정
		if(lineCount < fromPT) continue;
		if(lineCount >= fromPT+pageSize) break;
		
		if (writeDate.equals(today)){
			showNew="[new]";
		}
		
		if (relevel==0){
			showBar="";
			
%>
		<!-- 전체 조회 개인별 조회를 위한 링크 설정 -->
		<!-- 파라미터 id; name; 설정 -->
		<tbody >
		<tr >
			<td align=center style="overflow: hidden;"><%=id%></td>
			<td style="overflow: hidden;">
<%
			if (active==1) {
%>
				<a href="view.jsp?id=<%=id%>"><%=showBar%><%=title%> <%=showNew%></a></td>
<%
			} else if (active==0){
%>
				<font color="#848484"><%=showBar%> 삭제된 글 </font></a></td>
<%
			}
%>
			<td align=center style="overflow: hidden;"><%=writeDate%></td>
			<td align=center style="overflow: hidden;"><%=viewcnt%></td>
		</tr>
<%
		} else if (relevel > 0){			
			showBar="└>";
%>
		<!-- 전체 조회 개인별 조회를 위한 링크 설정 -->
		<!-- 파라미터 id; name; 설정 -->
		<tr >
			<td align=center style="overflow: hidden;"><%=id%></td>
			<td style="overflow: hidden;">
<%
			for(int i = 0; i < relevel; i++){
%>
				&emsp;
<%
			} 				
%>

<%
			if (active==1) {
%>
				<a href="view.jsp?id=<%=id%>"><%=showBar%><%=title%> <%=showNew%></a></td>
<%
			} else if(active==0){
%>
				<font color="#848484"><%=showBar%> 삭제된 글 </font></a></td>
<%
			}
%>
			<td align=center style="overflow: hidden;"><%=writeDate%></td>
			<td align=center style="overflow: hidden;"><%=viewcnt%></td>
		</tr>

<%
		}
	} 		
		// 자원 반환
		rset2.close();
		stmt2.close();
		conn.close();
%>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td align=right>
				<input type="button" class="btn btn-outline-dark" value="신규" OnClick=window.location='insert.jsp'>
			</td>
		</tr>
		</tbody>
</table>
</div>


<!-- 페이징 위한 행 삽입 -->	
<div class="container text-center">	
<div class="table-responsive-lg" style="width: device-width; height: device-height;">
<table width=1000 cellspacing="0" border="0" class="table table-borderless">
	<tr><td>
	<ul class="pagination justify-content-center">
		<li class="page-item">
		<a class="page-link" href="list.jsp?pageNum=<%=1%>" style="text-decoration:none">First</a>&nbsp&nbsp;
<%			
	if(currentGroup >= 1){
%>
		<li class="page-item disabled">
		<a class="page-link" aria-disabled="false" href="list.jsp?pageNum=<%=groupStartPage-1%>" style="text-decoration:none">Previous</a>&nbsp&nbsp;
<%		
	}

	int groupPageCount = groupSize;		// 그룹 당 페이지 수
	int index = groupStartPage;			// 현 그룹 시작 페이지	

	// 그룹당 페이지수가 0보다 크고 && 페이지 수가 그룹의 마지막 페이지 번호보다 작거나 같을때 반복문 실행
	while(groupPageCount>0 && index<=groupEndPage){				
%>
		<!-- <a href="wifi.jsp?pageNum=< %=index%>" style="text-decoration:none">< %=index%></a>&nbsp&nbsp; -->
<%	
		if(index==currentPage){
%>
			<li class="page-item">
			<b><a class="page-link" href="list.jsp?pageNum=<%=index%>" style="text-decoration:none">[<%=index%>]</a></b>
<%			
		} else {
%>
			<li class="page-item">
			<a class="page-link" href="list.jsp?pageNum=<%=index%>" style="text-decoration:none"><%=index%></a>
<%			
		}
		index = index+1;					// 페이지 번호 1씩 증가
		groupPageCount=groupPageCount-1;	// 읽어야 할 그룹 페이지 하나씩 감소
	}

	if(index<=totalPage){
%>
		<li class="page-item">
		<a class="page-link" href="list.jsp?pageNum=<%=index%>" style="text-decoration:none">Next</a>&nbsp&nbsp;
<%	
	}
	
	if(currentGroup >= 0){
%>
		<li class="page-item">
		<a class="page-link" href="list.jsp?pageNum=<%=totalPage%>" style="text-decoration:none">Last</a>&nbsp;
<%		
	}
%>
		</li>			
	</td>
	</tr>	
	</table>
	</div>
	
</center>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="/bootstrap-4.2.1-dist/js/bootstrap.js"></script>
</body>
</html>