<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*,javax.sql.*,java.io.*" %>

<html>
<head>
</head>
<body>
<center>
<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	// view.jsp 의 파라미터 값을 바탕으로 delete.jsp 실행
	String idS=request.getParameter("id");			// id
//	int id = Integer.parseInt(idS);
	String title=request.getParameter("title");		// title
	String path=request.getParameter("path");		// 파일이름
	
	String deleteTitle="삭제된 글";						// 삭제된 파일 제목
	String deleteContent="삭제된 글입니다.";				// 삭제된 파일 내용

	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");

	// Statement 연결 및 설정
	Statement stmt1 = conn.createStatement();
	
//	stmt1.execute("delete from gongji_table where id="+idS+";");
//	stmt1.execute("update gongji_table set title='"+deleteTitle+"', content='"+deleteContent+"', active=0 where id="+idS+";");

	// active 를 사용할 경우 데이터베이스에는 변화를 주지 않고 list 조회에서만 "삭제된글"임을 표시
	stmt1.execute("update gongji_table set active=0 where id="+idS+";");

	// 삭제할 파일을 삭제되었음을 표시
	
	// 파일 삭제를 위한 변수 선언 및 초기값 설정	
	String fileName=path;										// 파일 이름
	String dirPath="/var/lib/tomcat8/webapps/ROOT/upload/";		// 파일 저장 경로
	String filePath=dirPath+fileName;							// 전체 경로
	
	File f = new File(filePath);	// 파일 객체 생성
	if(f.exists()) {				// 파일이 존재할 경우 파일 삭제 실행
		f.delete();					// 파일 삭제
	}
	
//	stmt1.execute("set @cnt=0;");
//	stmt1.execute("update gongji set gongji.id=@cnt:=@cnt+1;");

	// 자원 반환
	stmt1.close();
	conn.close();
%>
<script language="javascript">
	location.href="list.jsp";
</script>
<center>
</body>
</html>