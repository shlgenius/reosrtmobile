<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>

<html>
<head>
<link rel="stylesheet" href="/bootstrap-4.2.1-dist/css/bootstrap.css">
<script src="/ckeditor/ckeditor.js"></script>

<script language="javascript">
	function submitForm(mode){
		if(mode=="reply"){
			fm.action="reply.jsp";
		} else if(mode=="delete"){
			fm.action="delete.jsp";
		} else if(mode=="update"){
			fm.action="update.jsp";
		}
		fm.submit();
	}
</script>
<!-- css 스타일 클래스 선언 : 버튼 스타일 -->
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
</style>
<style>

</style>
<style>
	.inputTitle {
		height: 20px; width: 400px;
	}
	.textArea {
		resize: none; height: 300px; width: 480px; 
	}
</style>
</head>
<body>
<%
	String loginOK = null;
	loginOK = (String)session.getAttribute("login_ok");
%>
<center>

<h1>이용 후기</h1><br>

<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	// list.jsp 의 id 값을 받기 위한 파라미터값 요청
	// ResultSet 의 결과값을 받기 위한 변수 선언 및 초기값 설정
	String idS=request.getParameter("id");
	int id = Integer.parseInt(idS);			// 문자열 형태를 정수형 변수로 변환하여 변수값을 저장	
	String title="";		// 글제목
	String writeDate="";	// 입력일자
	String content="";		// 내용
	int rootid=0;			// 원글번호
	int relevel=0;			// 댓글레벨
	int recnt=0;			// 댓글 내 표시 순서
	int viewcnt=0;			// 조회수
	
	String filePath="";

	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();

	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from gongji_table where id="+id+";");

	// ResultSet 이 존재하는 동안 이하 반복문 실행
	while(rset1.next()){
		
		title=rset1.getString(2);
		writeDate=rset1.getString(3);
		content=rset1.getString(4);
		rootid=rset1.getInt(5);
		relevel=rset1.getInt(6);
		recnt=rset1.getInt(7);
		viewcnt=rset1.getInt(8);
		
		viewcnt=viewcnt+1;
		
		filePath=rset1.getString(9);
	}
	
	String sql = "update gongji_table set viewcnt="+viewcnt+" where id="+id;
	stmt1.execute(sql);
	
	// 자원 반환
	rset1.close();
	stmt1.close();
	conn.close();
%>
<div class="container text-center">
<!-- form 선언 : post 방식; name='fm' -->
<form method=post name="fm">
<!-- 결과값 출력 -->
<table width=800 cellspacing="1" border="1" class="table table-bordered">
	<tr>
		<td width=15% >번호</td>
		<td><%=id%><input type=hidden name="id" value=<%=id%>></td>
	</tr>
	<tr>
		<td >제목</td>
		<td><%=title%><input type=hidden name="title" value=<%=title%>></td>
	</tr>
	<tr>
		<td >일자</td>
		<td><%=writeDate%></td>
	</tr>
	<tr>
		<td >내용</td>
		<!--	<td><textarea name="content" cols=70 rows=300 class="textArea"> 변수값 content </textarea></td>		-->
		<td style="padding-left:0px">
			<textarea id="editor1" name="content" cols=70 rows=300 class="textArea" readonly><%=content%></textarea>
			<script>
				var cont='';
				//ckeditor라는 오픈소스 에디터를 넣는다.
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                // CKEDITOR.replace( 'content' );
				var editor = CKEDITOR.replace( 'content', {hegiht: 300});

				// The "change" event is fired whenever a change is made in the editor.
				// var that = this;
				editor.on( 'change', function( evt ) {
					// getData() returns CKEditor's HTML content.
					cont = evt.editor.getData();
					console.log( 'Total bytes: ' + evt.editor.getData().length );
				});
            </script>
			<script>
				// $(document).ready(function() {
					// $('#summernote').summernote({
						// tabsize: 2,
						// height: 300,
					// });
				// });
			</script>
		</td>
	</tr>
	<tr >
		<td >첨부파일</td>
<%
	if(filePath==null){
%>		
		<td >파일이 없습니다.</td>	
<%
	} else if (filePath.equals("null")){
%>	
		<td >파일이 없습니다.</td>	
<%
	} else {
%>
		<td >
			<img src="/upload/<%=filePath%>" width=360>
			<input type=hidden name="path" value=<%=filePath%>></input>
		</td>		
<%	
	}
%>
	</tr>
	<tr>
		<td >조회수</td>
		<td><%=viewcnt%></td>
	</tr>
	<tr>
		<td >원글번호</td>
		<td><%=rootid%><input type=hidden name="rootid" value=<%=rootid%>></td>
	</tr>
	<tr>
		<td >댓글수준</td>
		<td><%=relevel%><input type=hidden name="relevel" value=<%=relevel%>></td>
	</tr>
		<td>댓글내 순서</td>
		<td><%=recnt%><input type=hidden name="recnt" value=<%=recnt%>></td>		
	</tr>
	
</table>
<br>
</div>

<div class="container text-center">
<!-- 파일 이동 위한 버튼 출력 및 설정 : 목록; 수정 -->
<table width=800 cellspacing=1 border=0 class="table table-borderless">
	<tr>
		<td width=60%>
		</td>
		<td width=10% align=left>
<%
	if(loginOK==null){
%>
		</td>	
<%
	} else if (loginOK.equals("yes")){
%>
		<input type="button" class="btn btn-outline-dark" value="수정" OnClick="submitForm('update');"></input></td>		
<%
	}
%>
		<td width=10% align=left>
<%
	if(loginOK==null){
%>
		</td>	
<%
	} else if (loginOK.equals("yes")) {
%>
		<input type="button" class="btn btn-outline-dark" value="삭제" OnClick="submitForm('delete');"></input></td>
<%
	}
%>
		
		<td width=10% align=left>
			<input type="button" class="btn btn-outline-dark" value="목록" OnClick=location.href="list.jsp"></input>
		</td>
		<td width=10% align=left>
			<input type="button" class="btn btn-outline-dark" value="댓글" OnClick="submitForm('reply');"></input>
		</td>
	</tr>
</table>
</div>
</form>
</center>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="/bootstrap-4.2.1-dist/js/bootstrap.js"></script>
</body>
</html>