<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="java.sql.*, javax.sql.*, java.io.*, java.text.*, java.util.*" %>

<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>

<html>
<head>
</head>
<body>
<h1>이용 후기</h1><br>
<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	Statement stmt1 = conn.createStatement();	
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
%>

<% 
	String keyS=request.getParameter("key");	
%>

<%	
	if(keyS.equals("INSERT")){	

		// request.getRealPath();를 출력한 결과
		// /var/lib/tomcat8/webapps/ROOT/ 
		// 여기에 파일을 업로드할 디렉토리를 지정
		String uploadPath=request.getRealPath("/upload");
	
		int size = 5*512*512;	// 그림파일 최대크기 지정	
			
		// MultipartRequest는 
		// page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"
		// page import="com.oreilly.servlet.MultipartRequest"
		// 를 요구한다.
		// 또한 기존 입력 폼에서 enctype="multipart/form-data"를 사용하면 request를 multi로 받아야 한다.
		MultipartRequest multi=new MultipartRequest(request,uploadPath,size,"UTF-8",new DefaultFileRenamePolicy());
		// DefaultFileRenamePolicy는 파일 이름 중복일 경우 파일 이름 재설정
		
		String filename1="";	
		String title=multi.getParameter("title");
		String writeDate=multi.getParameter("writeDate");
		String content=multi.getParameter("content");	
		
		Enumeration files = multi.getFileNames();	// Enumeration: page import="java.util.*" 를 요구한다.
		String file1 = (String)files.nextElement();
		filename1 = multi.getFilesystemName(file1);		
		
		stmt1.execute("insert into gongji_table (title,date,content,relevel,recnt,viewcnt,filepath,active) values('"+title+"','"+writeDate+"','"+content+"', 0, 0, 0,'"+filename1+"',1);");
		
		// 새글을 작성한 경우에는 rootid를 id와 동일하게 업데이트 
		ResultSet rset2 = stmt2.executeQuery("select id,rootid from gongji_table;");
		while(rset2.next()){		
			if(rset2.getInt(2) == 0){//새 글 등록시 댓글이 아니면 rootid등록.
				int id = rset2.getInt(1);
				String sql = "update gongji_table set rootid="+id+" where id="+id;
				stmt3.execute(sql);
		}
	}
		
	} else if (keyS.equals("UPDATE")){	

		String idS = request.getParameter("id");
		String content=request.getParameter("content");	
	
		stmt1.execute("update gongji_table set content='"+content+"' where id="+idS+";");
		
	} else if (keyS.equals("REPLY")){
		
		String idS = request.getParameter("id");
		String title=request.getParameter("title");
		String writeDate=request.getParameter("writeDate");
		String content=request.getParameter("content");	
		
		String rootidS=request.getParameter("rootid");
		int rootid=Integer.parseInt(rootidS);
		String relevelS=request.getParameter("relevel");
		int relevel=Integer.parseInt(relevelS);
		String recntS=request.getParameter("recnt");
		int recnt=Integer.parseInt(recntS);
		
		stmt1.execute("insert into gongji_table (title,date,content,rootid,relevel,recnt,active) values('"+title+"','"+writeDate+"','"+content+"','"+rootid+"','"+relevel+"','"+recnt+"',1);");
	}
	
//	stmt1.execute("set @cnt = 0;");
//	stmt1.execute("update gongji set gongji.id=@cnt:=@cnt+1;");
	
	stmt2.close();
	stmt1.close();
	conn.close();
%>

<script language="javascript">
	location.href="list.jsp";
</script>
</body>
</html>