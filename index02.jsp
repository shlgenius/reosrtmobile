<%@ page contentType="text/html; charset=utf-8" language="java" %>
<!-- 필요한 부분 import -->
<%@ page import="java.sql.*,javax.sql.*,java.io.*,java.text.*, java.util.*"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="user-scable=no, width=device-width, initial-scale=1.0;"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<script language='javascript' type='text/javascript'>
	var orientationEvent;
	var uAgent=navigator.userAgent.toLowerCase();
	var mobilePhones='android';
	if(uAgent.indexOf(mobilePhones)!=-1){
		orientationEvent="resize";//안드
	}
	else orientationEvent="orientationchange";//아이폰
//		window.addEventListener(orientationEvent,function(){location.href('#');},false);

	var prevScreen=-1;
	var sv_prevScreen=0;
	function prevShow()
	{
		ScreenShow(prevScreen);
	}

	var muCnt =5;//서브메뉴
	var scCnt = 15;//화면

	function fncShow(pos)
	{
		var i =0;
		for(i=0;i<scCnt;i++)
		{
			var obj = document.getElementById("s"+i);
			obj.style.display = 'none';
		}
		for(i=0;i<muCnt;i++)
		{
			var obj = document.getElementById("menu"+i);
			var obj2 = document.getElementById("m"+i);
			if(i==pos)
			{
				obj.style.display ='';
//					obj2.style.background="#A4A4A4";
					obj2.style.background="#A4A4A4";
			}
			else
			{
				obj.style.display = 'none';
//					obj2.style.background="#D8D8D8";
					obj2.style.background="#FFFFFF";
			}
		}
	}

	var scCnt = 14;
	var ScrObj;
	var timer1;
	function ScrAnimation(){
		var offset = -50;
			
			if(parseInt(ScrObj.style.left) > 10)
			{
				ScrObj.style.left = parseInt(ScrObj.style.left) + offset + "px";
				timer1 = setTimeout("ScrAnimation()",1);
			} 
			else
			{
				ScrObj.style.left=5;
				clearTimeout(timer1);
			}
	}
	function ScreenShow(pos)
	{
		var i = 0;
	
		//메뉴페이지 막기
		for(i=0;i<muCnt;i++)
		{
			var obj =document.getElementById("menu"+i);
				obj.style.display = 'none';
		}
		for(i=0;i<scCnt;i++)
		{
			var obj = document.getElementById("s"+i);
			if(i==pos)
			{
				prevScreen = sv_prevScreen;
				sv_prevScreen=i;
				obj.style.display = '';
				obj.style.position="relative";
				obj.style.top=35;
				obj.style.left=screen.width;
				obj.style.height=screen.height-120;
				
				ScrObj=obj;
				ScrAnimation();
			}
			else
			{
				obj.style.display = 'none';
			}
		}
	}
</script>

<style type="text/css">
li {text-align: left; vertical-align: middle; margin: 4; padding: 10; height: 5%; background-color: #ffffff; border: 2px solid black; color: #000000; font-size: 16px;}
ul {text-align: left; vertical-align: middle; margin: 4; padding: 10; height: 5%; background-color: #ffffff; border: 2px solid black; color: #000000; font-size: 16px;}
</style>
</head>
<body onload='ScreenShow(0);'>
<center>
	<div id="container" style="width:device-width;height:device-height;">
		<div id="header1" style="background-color:#ffffff;height:35px;width:15%;float:left;" onclick='prevShow();'>
			<center>
				<img src="pic/btn_back01.png" width=30px height=32px>
			</center>
		</div>
		<div id="header2" style="background-color:#ffffff;height:35px;width:70%;float:left;">
			<center style="margin:10px; font-color:white;"><B>조아리조트</B></center>
		</div>
		<div id="header3" style="background-color:#ffffff;height:35px;width:15%;float:left;" onclick='ScreenShow(0);'>
			<center>
				<img src="pic/iconmonstr-home-6-240.png" width=30px height=32px>
			</center>
		</div>
	
		<div id="menu0" style="background-color:#ffffff;display:none;width:device-width">
			<div style="height: 9%; width: 90%; border: 0px solid #000000;"></div>
			<div style="background-color: #000000; width: 90%; padding: 3% 3%; border: 1px solid #000000; text-align: center; color: #ffffff;">리조트 소개</div>
				<div onclick='ScreenShow(0);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">리조트</div>
				<div onclick='ScreenShow(1);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">VIP</div>
				<div onclick='ScreenShow(2);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">일반</div>
				<div onclick='ScreenShow(3);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">합리</div>
		</div>
		<div id="menu1" style="background-color:#ffffff;display:none;width:device-width">			
			<div style="height: 9%; border: 0px solid #000000;"></div>
			<div style="background-color: #000000; width: 90%; padding: 3% 3%; border: 1px solid #000000; text-align: center; color: #ffffff;"><b>주변명소</b></div>				
				<div onclick='ScreenShow(4);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">찾아오는길</div>
				<div onclick='ScreenShow(5);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">대중교통이용</div>
				<div onclick='ScreenShow(6);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">자가용이용</div>				
		</div>
		<div id="menu2" style="background-color:#ffffff;display:none;width:device-width">
			<div style="height: 9%; border: 0px solid #000000;"></div>
			<div style="background-color: #000000; width: 90%; padding: 3% 3%; border: 1px solid #000000; text-align: center; color: #ffffff;"><b>주변명소</b></div>				
				<div onclick='ScreenShow(7);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">근처 산</div>
				<div onclick='ScreenShow(8);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">해수욕장</div>
				<div onclick='ScreenShow(9);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">오온처언</div>
		</div>
		<div id="menu3" style="background-color:#ffffff;display:none;width:device-width;">
			<div style="height: 9%; border: 0px solid #000000;"></div>
			<div style="background-color: #000000; width: 90%; padding: 3% 3%; border: 1px solid #000000; text-align: center; color: #ffffff;"><b>예약하기</b></div>				
				<div onclick='ScreenShow(10);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">예약현황</div>
				<div onclick='ScreenShow(11);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">예약하기</div>				
		</div>
		<div id="menu4" style="background-color:#ffffff;display:none;width:device-width">
			<div style="height: 9%; border: 0px solid #000000;"></div>
			<div style="background-color: #000000; width: 90%; padding: 3% 3%; border: 1px solid #000000; text-align: center; color: #ffffff;"><b>리조트소식</b></div>				
				<div onclick='ScreenShow(12);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">리조트소식</div>
				<div onclick='ScreenShow(13);' style="background-color: #ffffff; width: 90%; margin: 4; padding: 3% 3%; border: 1px solid #000000; text-align: left; color: #000000;">이용후기</div>				
		</div>
	
	<div id="s0" style="display: none; width: device-width;">
		<center>
			<img src="pic/hotel01.jpg" width=60% height=36%></img>
			<br>조아리조트에서 나가라
		</center>
	</div>
	<div id="s1" style="display: none; width: device-width;">
		<center>
			<img src="pic/room04.jpg" width=200px height=150px></img>
			<br>VIP
		</center>
	</div>
	<div id="s2" style="display: none; width: device-width;">
		<center>
			<img src="pic/room05.jpg" width=200px height=150px></img>
			<br>일반
		</center>
	</div>
	<div id="s3" style="display: none; width: device-width;">
		<center>
			<img src="pic/room06.jpg" width=200px height=150px></img>
			<br>합리
		</center>
	</div>	
	
	<div id="s4" style="display: none;">		
		<br>
		<iframe width='80%' height='50%' id='mapcanvas' src='https://maps.google.com/maps?q=%EA%B2%BD%EA%B8%B0%EB%8F%84%20%EC%84%B1%EB%82%A8%EC%8B%9C%20%EB%B6%84%EB%8B%B9%EA%B5%AC%20%EC%84%9C%ED%98%84%EC%97%AD&amp;t=&amp;z=10&amp;ie=UTF8&amp;iwloc=&amp;output=embed' frameborder='0' scrolling='no' marginheight='0' marginwidth='0'>
			<div style='overflow:hidden;'>
				<div id='gmap_canvas' style='height:100%;width:100%;'>
					<div class="zxos8_gm">
						<a rel="nofollow"  href="https://wildernesswood.co.uk/group-visits/">Wilderness Wood</a>
					</div>
				</div>
			</div>
			<div>
				<small>Powered by <a href="https://www.embedgooglemap.co.uk">Embed Google Map</a></small>
			</div>
		</iframe>
		<br>※ 이곳으로 오세요
	</div>
	<div id="s5" style=" display:none;">
		<br>
		<iframe width='80%' height='50%' id='mapcanvas' src='https://maps.google.com/maps?q=%EA%B2%BD%EA%B8%B0%EB%8F%84%20%EC%84%B1%EB%82%A8%EC%8B%9C%20%EB%B6%84%EB%8B%B9%EA%B5%AC%20%EC%84%9C%ED%98%84%EC%97%AD&amp;t=&amp;z=10&amp;ie=UTF8&amp;iwloc=&amp;output=embed' frameborder='0' scrolling='no' marginheight='0' marginwidth='0'>
			<div style='overflow:hidden;'>
				<div id='gmap_canvas' style='height:100%;width:100%;'>
					<div class="zxos8_gm">
						<a rel="nofollow"  href="https://wildernesswood.co.uk/group-visits/">Wilderness Wood</a>
					</div>
				</div>
			</div>
			<div>
				<small>Powered by <a href="https://www.embedgooglemap.co.uk">Embed Google Map</a></small>
			</div>
		</iframe>	
		<br>지하철 이용 시
		<br> 7호선 어린이대공원역 하차 후 6번 출구
		<br> 5호선 군자역 세종초등학교 방면 7번 출구
		<br>※ 세종대학교 후문 건너편 흰색 높은 건물 102호(새날관)<br>
		<br>일반버스 이용시
		<br> - 어린이대공원 : 간선 721  지선 4212, 3216
		<br> - 화양리 : 간선 240, 302  지선 2016,2222,3217,3229
		<br> - 세종사이버대학교정문(세종초등학교앞) : 간선 240  지선 2012,2016
	</div>
	<div id="s6" style=" display:none;">
		<br>
		<iframe width='80%' height='50%' id='mapcanvas' src='https://maps.google.com/maps?q=%EA%B2%BD%EA%B8%B0%EB%8F%84%20%EC%84%B1%EB%82%A8%EC%8B%9C%20%EB%B6%84%EB%8B%B9%EA%B5%AC%20%EC%84%9C%ED%98%84%EC%97%AD&amp;t=&amp;z=10&amp;ie=UTF8&amp;iwloc=&amp;output=embed' frameborder='0' scrolling='no' marginheight='0' marginwidth='0'>
			<div style='overflow:hidden;'>
				<div id='gmap_canvas' style='height:100%;width:100%;'>
					<div class="zxos8_gm">
						<a rel="nofollow"  href="https://wildernesswood.co.uk/group-visits/">Wilderness Wood</a>
					</div>
				</div>
			</div>
			<div>
				<small>Powered by <a href="https://www.embedgooglemap.co.uk">Embed Google Map</a></small>
			</div>
		</iframe>
		<center>
		<br>자가용 이용 시
		<br> 영동대교 이용시 : 영동대교 화양사거리(우회전) 화양삼거리(좌회전) 조아리조트
		<br> 올림픽대교 이용시 : 올림픽대교 화양사거리(직진) 화양삼거리(우회전) 조아리조트
		<br> 군자교 이용시 : 군자교 면목로(우회전) 조아리조트
		<br>
		<br>※ 주차는 조아리조트 건물 뒤편으로 진입하여 조아리조트 주차장을 이용하시면 됩니다.
	</div>
	
	<div id="s7" style=" display:none;">
		<img src= "pic/mountain01.jpg" width=200px height=150px>
		<br>근처에 산
	</div>
	<div id="s8" style=" display:none;">
		<img src= "pic/beach01.jpg" width=200px height=150px>
		<br>해수욕장
	</div>
	<div id="s9" style=" display:none;">
		<img src= "pic/hotspring01.jpg" width=200px height=150px>
		<br>온천
	</div>	
	
	<div id="s10" style=" display:none;">
		<iframe src="reservation/d_01.jsp" frameborder="0" border="0" bordercolor="white" width=100% height=80% marginwidth="0" marginheight="0" scroling="yes"></iframe>
	</div>
	<div id="s11" style=" display:none;">
		<iframe src="reservation/d_02.jsp" frameborder="0" border="0" bordercolor="white" width=100% height=80% marginwidth="0" marginheight="0" scroling="yes"></iframe>
	</div>
	
	<div id="s12" style=" display:none;">
		<iframe src="notice/gongji_list.jsp" frameborder="0" border="0" bordercolor="white" width=100% height=80% marginwidth="0" marginheight="0" scroling="yes"></iframe>
	</div>
	<div id="s13" style=" display:none;">
		<iframe src="review/list.jsp" frameborder="0" border="0" bordercolor="white" width=100% height=80% marginwidth="0" marginheight="0" scroling="yes"></iframe>
	</div>
		
	
	<div id="m0" onclick='fncShow(0);' style="position: fixed; bottom: 0px; position: fixed; left: 1%; background-color: #ffffff; height: 60px; width: 20%; float: left; font-size: 70%;">
		<center>
			<img src="pic/iconmonstr-building-21-240.png" width=40px height=40px><br>
			리조트소개
		</center>
	</div>
	<div id="m1" onclick='fncShow(1);' style="position: fixed; bottom: 0px; position: fixed; left: 21%; background-color: #ffffff; height: 60px; width: 20%; float: left; font-size: 70%;">
		<center>
			<img src="pic/baseline_place_black_18dp.png" width=40px height=40px><br>
			찾아오기
		</center>
	</div>
	<div id="m2" onclick='fncShow(2);' style="position: fixed; bottom: 0px; position: fixed; left: 41%; background-color: #ffffff; height: 60px; width: 20%; float: left; font-size: 70%;">
		<center>
			<img src="pic/baseline_terrain_black_18dp.png" width=40px height=40px><br>
			주변명소
		</center>
	</div>
	<div id="m3" onclick='fncShow(3);' style="position: fixed; bottom: 0px; position: fixed; left: 61%; background-color: #ffffff; height: 60px; width: 20%; float: left; font-size: 70%;">
		<center>
			<img src="pic/iconmonstr-credit-card-1-240.png" width=40px height=40px><br>
			예약하기
		</center>
	</div>
	<div id="m4" onclick='fncShow(4);' style="position: fixed; bottom: 0px; position: fixed; left: 81%; background-color: #ffffff; height: 60px; width: 20%; float: left; font-size: 70%;">
		<center>
			<img src="pic/iconmonstr-speech-bubble-33-240.png" width=40px height=40px><br>
			리조트소식
		</center>
	</div>
	
</div>
</center>
</body>
</html>