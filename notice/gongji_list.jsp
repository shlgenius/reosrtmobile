<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>

<html>
<head>
<link rel="stylesheet" href="/bootstrap-4.2.1-dist/css/bootstrap.css">

<!-- css 스타일 선언 : a 링크 스타일 -->
<style>
	a:link { color: #000000; text-decoration: none; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #000000; text-decoration: underline; font-weight: none;}
	a:active { color: #000000; text-decoration: none; font-weight: bold;}
</style>
</head>
<body>
<center>
<h4><b>리조트소식</b></h4><br>
<%
	String loginOK = null;
	loginOK = (String)session.getAttribute("login_ok");
	
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// stmt 생성 및 연결 설정
	Statement stmt = conn.createStatement();
			
	// ResultSet 선언 및 설정
	ResultSet rset = stmt.executeQuery("select * from gongji order by id desc;");			
%>

<div class="container text-center" style="width: device-width; height: device-height;">
<table class="table" style="table-layout: fixed; white-space: nowrap; font-size: 85%; word-break:break-all;">
	<thead class="thead-dark">
	<tr align=center>
		<th scope="col" width=15%>번호</th>
		<th scope="col" width=65%>제목</th>
		<th scope="col" width=20%>등록일</th>
	</tr>
	</thead>
	<tbody >
<%
	// ResultSet 이 존재하는 동안 이하 반복문 실행	
	while(rset.next()){
%>
		<!-- 전체 조회 개인별 조회를 위한 링크 설정 -->
		<!-- 파라미터 id; name; 설정 -->
		
		<tr >
			<td align=center><%=Integer.toString(rset.getInt(1))%></td>
			<td ><a href="gongji_view.jsp?id=<%=rset.getInt(1)%>"><%=rset.getString(2)%></a></td>			
			<td align=center><%=rset.getString(3)%></td>						
		</tr>
		
<%
		} 		
		// 자원 반환
		rset.close();
		stmt.close();
		conn.close();
%>
</tbody>
</table>
</div>
</div>
<br>
<!-- 버튼용 테이블 생성 -->
<div class="container text-center">	
<table width=800 cellspacing=1 border=0 class="table table-borderless">
	<tr height=30>
		<td width=60%></td>		
		<td width=10% align=left></td>
		<td width=10% align=left></td>
		<td width=10% align=left></td>
<%
	if(loginOK==null){
%>
		<td width=10%>
		</td>		
<%
	} else if (loginOK.equals("yes")) {
%>
		<td width=10% align=left>			
			<input type="button" class="btn btn-outline-dark" value="신규" OnClick="window.location='gongji_insert.jsp'">			
		</td>
<%	
	}
%>
	</tr>
</table>
</div>
</center>	
</body>
</html>