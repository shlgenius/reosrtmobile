<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.io.*" %>

<html>
<head>
<link rel="stylesheet" href="/bootstrap-4.2.1-dist/css/bootstrap.css">
<script src="/ckeditor/ckeditor.js"></script>

<!-- css 스타일 클래스 선언 : 버튼 스타일 -->
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
</style>
<!-- css 스타일 클래스 선언 : input, textArea 스타일 -->
<style>
	.inputTitle {
		height: 20px; width: 400px;
	}
	.textArea {
		resize: none; height: 300px; width: 480px; 
	}
</style>
</head>
<body>
<center>
<h1>리조트 소식</h1><br>
<!-- 데이터베이스 연결; 변수 선언; 결과값 저장; -->
<%
	String loginOK = null;
	loginOK = (String)session.getAttribute("login_ok");	

	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	// gongji_list.jsp 의 id 값을 받기 위한 파라미터값 요청
	// ResultSet 의 결과값을 받기 위한 변수 선언 및 초기값 설정
	String idS=request.getParameter("id");
	int id = Integer.parseInt(idS);			// 문자열 형태를 정수형 변수로 변환하여 변수값을 저장	
	String title="";
	String writeDate="";
	String content="";

	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();

	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from gongji where id="+id+";");

	// ResultSet 이 존재하는 동안 이하 반복문 실행
	while(rset1.next()){
		title=rset1.getString(2);
		writeDate=rset1.getString(3);
		content=rset1.getString(4);				
	}
	// 자원 반환
	rset1.close();
	stmt1.close();
	conn.close();
%>

<div class="container text-center">
<!-- 결과값 출력 -->
<table width=800 cellspacing="1" border="1" class="table table-bordered">
	<tr >
		<td width=15% >번호</td>
		<td><%=id%></td>
	</tr>
	<tr >
		<td >제목</td>
		<td><%=title%></td>
	</tr>
	<tr >
		<td >일자</td>
		<td><%=writeDate%></td>
	</tr>
	<tr >
		<td >내용</td>
		<!--	<td><textarea name="content" cols=70 rows=300 class="textArea"> 변수값 content </textarea></td>		-->
		<td ><%=content%>
<!--		<textarea id="editor1" name="content" cols=70 rows=300 class="textArea" readonly></textarea>	-->
			<script>
				var cont='';
				//ckeditor라는 오픈소스 에디터를 넣는다.
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                // CKEDITOR.replace( 'content' );
				var editor = CKEDITOR.replace( 'content', {hegiht: 300});

				// The "change" event is fired whenever a change is made in the editor.
				// var that = this;
				editor.on( 'change', function( evt ) {
					// getData() returns CKEditor's HTML content.
					cont = evt.editor.getData();
					console.log( 'Total bytes: ' + evt.editor.getData().length );
				});
            </script>
			<script>
				// $(document).ready(function() {
					// $('#summernote').summernote({
						// tabsize: 2,
						// height: 300,
					// });
				// });
			</script>
		</td>
	</tr>
</table>
</div>
<br>


<div class="container text-center">
<!-- 파일 이동 위한 버튼 출력 및 설정 : 목록; 수정 -->
<table width=800 cellspacing=1 border=0 class="table table-borderless">
	<tr >
		<td width=60%></td>		
		<td width=10% align=left></td>
		<td width=10% align=left></td>		
		
<%
	if(loginOK==null){
%>
		<td width=10%>
		</td>
		<td width=10 align=left>
			<input type="button" class="btn btn-outline-dark" value="목록" OnClick=location.href="gongji_list.jsp">
		</td>
<%
	} else if (loginOK.equals("yes")) {
%>
		<td width=10 align=left>
			<input type="button" class="btn btn-outline-dark" value="목록" OnClick=location.href="gongji_list.jsp">
		</td>
		<td width=10% align=left>			
			<input type="button" class="btn btn-outline-dark" value="수정" OnClick=location.href="gongji_update.jsp?id=<%=id%>">		
		</td>
<%	
	}
%>
	</tr>
</table>
</div>
</center>
</body>
</html>