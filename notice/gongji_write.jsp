<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.io.*" %>

<html>
<head>
</head>
<body>
<h1>리조트 소식</h1><br>
<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	// gongji_insert.jsp 에서 form-action으로 gongji_write.jsp 실행
	// gongji_update.jsp 에서 form-action으로 gongji_write.jsp 실행

	// parameter name="id", value="id" 를 받는다	
	// parameter name="title", value="title" 를 받는다	
	// parameter name="writeDate", value="writeDate" 를 받는다	
	// parameter name="content", value="content" 를 받는다	
	
	// parameter 값을 바탕으로 gongji_write.jsp를 실행	
	
	String keyS=request.getParameter("key");
	String idS = request.getParameter("id");
//	int id=Integer.parseInt(idS);
	String title=request.getParameter("title");
	String writeDate=request.getParameter("writeDate");
	String content=request.getParameter("content");

	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();
	
	// key 값을 바탕으로 이하 실행
	if(keyS.equals("INSERT")){
		stmt1.execute("insert into gongji(title, date, content) values ('"+title+"','"+writeDate+"','"+content+"')");
	} else if (keyS.equals("UPDATE")){
		stmt1.execute("update gongji set title='"+title+"', content='"+content+"' where id="+idS+";");
	}
	
	// 입력 후 강제로 번호 재지정 sql 명령
	stmt1.execute("set @cnt = 0;");
	stmt1.execute("update gongji set gongji.id=@cnt:=@cnt+1;");

	// 자원반환
	stmt1.close();
	conn.close();
%>
<!-- jsp 실행 후 보이는 화면이 gongji_list.jsp 화면 -->
<script language="javascript">
	location.href="gongji_list.jsp";
</script>
</body>
</html>