<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text.html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.io.*" %>

<html>
<head>
<link rel="stylesheet" href="/bootstrap-4.2.1-dist/css/bootstrap.css">
<script src="/ckeditor/ckeditor.js"></script>

<!-- include libraries(jQuery, bootstrap) -->
<!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet"> -->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>  -->
<!-- <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> --> 

<!-- include summernote css/js -->
<!-- <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet"> -->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script> -->

<!-- css 스타일 클래스 선언 : input, textArea 스타일 -->
<style>
	.inputTitle {
		height: 20px; width: 400px;
	}
	.textArea {
		resize: none; height: 300px; width: 480px; 
	}
</style>

<!-- 정규식 활용하여 입력 제한 -->
<script language="javascript">
	function submitForm(model){
		
		var title = document.forms[0].title.value;	
				
		var han=/^[가-힣]*$/;
		var eng=/^[a-zA-Z]*$/;
		var pattern_name=/^[가-힣a-zA-Z0-9]{1,10}$/gi;
		var pattern_special=/[~!^{}<>;\/?]/gi;		
		var pattern_blank = /^\s+|\s+$/g;
		
		if (title==null || title==""){
			alert("입력 요구");
			document.forms[0].title.focus();
			return false;
		}
		if(pattern_blank.test(title)==true ){
			alert("앞 뒤 공백 입력 불가");
			document.forms[0].title.focus();
			return false;
		}	
		if (pattern_name.test(title)==false && pattern_special.test(title)==true){
			alert("잘못된 입력");
			document.forms[0].title.value=title;
			document.forms[0].title.focus();
			return false;
		}
		
		if(model=="update"){
			fm.action="gongji_write.jsp?key=UPDATE";
		} else if(model=="delete"){
			fm.action="gongji_delete.jsp";
		}
		fm.submit();
	}
</script>
<script language="JavaScript">
<!-- onkeydown 이벤트를 사용해서 탭키를 삽입 -->
<!-- <textArea onkeydown="useTab(this)"></textarea> -->
	// function useTab(obj){
		// if(event.keyCode == 9){
			// var tab = '\t';
			// obj.selection = document.selection.createRange();
			// obj.selection.text = tab;
			// event.returnValue = false;
		// }
	// }
</script>
</head>
<body>
<center>
<h1>리조트 소식</h1><br>
<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	// gongji_view.jsp 에서 "수정" 실행시 폼-액션으로 gongji_update.jsp 실행
	// parameter name="id", value="id" 를 받는다	
	// parameter 값을 바탕으로 gongji_update.jsp를 실행	
	// parameter 값을 정수형 변환
	String idS=request.getParameter("id");
	int id=Integer.parseInt(idS);
	
	// 변수 선언 및 초기값 설정
	String title="";
	String writeDate="";
	String content="";

	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();
	
	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from gongji where id="+id+";");	

	// ResultSet 이 존재하는 동안 이하 반복문 실행
	while(rset1.next()){
		title=rset1.getString(2);
		writeDate=rset1.getString(3);
		content=rset1.getString(4);
	}
	
	// 자원 반환
	rset1.close();
	stmt1.close();
	conn.close();
%>
<!-- form 선언 : post 방식; name='fm' -->
<form method=post name="fm">
<!-- 수정 폼 -->
<!-- 수정 실행시 id; title; 를 파라미터 값으로 전달 -->

<div class="container text-center">	
<table width=800 cellspacing=1 border=1 class="table table-bordered">
	<tr >
		<td width=15%>번호</td>
		<td><%=id%><input type=hidden name="id" value=<%=id%>></input></td>
	</tr>
	<tr >
		<td>제목</td>
		<td><input type=text class="form-control" name="title" value=<%=title%>></input></td>
	</tr>
	<tr >
		<td>일자</td>
		<td><%=writeDate%></td>
	</tr>
	<tr>
		<td>내용</td>
<!--	<td><textarea name="content" cols=70 rows=300 class="textArea"> 변수값 content </textarea></td>		-->
		<td style="padding-left:0px">
			<textarea id="editor1" name="content" cols=70 rows=300 class="textArea"><%=content%></textarea>
			<script>
				var cont='';
				//ckeditor라는 오픈소스 에디터를 넣는다.
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                // CKEDITOR.replace( 'content' );
				var editor = CKEDITOR.replace( 'content', {hegiht: 300});

				// The "change" event is fired whenever a change is made in the editor.
				// var that = this;
				editor.on( 'change', function( evt ) {
					// getData() returns CKEditor's HTML content.
					cont = evt.editor.getData();
					console.log( 'Total bytes: ' + evt.editor.getData().length );
				});
            </script>
			<script>
				// $(document).ready(function() {
					// $('#summernote').summernote({
						// tabsize: 2,
						// height: 300,
					// });
				// });
			</script>
		</td>
	</tr>
</table>
</div>
<br>

<div class="container text-center">	
<!-- 버튼용 테이블 생성 -->
<table width=800 cellspacing=1 border=0 class="table table-borderless">
	<tr height=30>
		<td width=60%></td>		
		<td width=10% align=left></td>		
		<td width=10% align=left><input type="button" class="btn btn-outline-dark" value="취소" OnClick=location.href="gongji_list.jsp"></input></td>
		<td width=10% align=left><input type="button" class="btn btn-outline-dark" value="쓰기" OnClick="submitForm('update');"></input></td>
		<td width=10% align=left><input type="button" class="btn btn-outline-dark" value="삭제" OnClick="submitForm('delete');"></input></td>
	</tr>
</table>
</div>
</form>
</center>
</body>
</html>