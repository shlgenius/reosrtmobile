<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*,javax.sql.*,java.io.*"%>
<html>
<head>
</head>
<body>
<h1>Drop gongji_table<br>
<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	//stmt 생성 후 연결
	Statement stmt = conn.createStatement();
	
	// 예외처리
	try{
		
		// 쿼리문 실행 : 테이블 드랍
		stmt.execute("drop table joaresv;");
		out.println("OK");
		
	// 예외 발생시 출력문 실행
	} catch(SQLException e){
		out.println("실패 - 테이블이 존재하지 않습니다.");
	}
	
	// 자원반환
	stmt.close();
	conn.close();
%>
</h1>
</body>
</html>