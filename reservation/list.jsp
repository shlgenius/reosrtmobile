<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*,javax.sql.*,java.io.*,java.text.*, java.util.*"%>

<html>

<head>
<!-- css 스타일 선언 : a 링크 스타일 -->
<style>	
</style>
</head>
<body>
<center>
<h1>d_01.jsp LIST</h1> <br>
<%
	Calendar cal = Calendar.getInstance();
	
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.KOREA);
	df.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
		
	SimpleDateFormat df2 = new SimpleDateFormat("EEE",Locale.KOREA);
	df2.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
	
	String today = df.format(new java.util.Date());//현재 날짜
	
//	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd",Locale.KOREA);
//	formatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
//	String today = formatter.format(new java.util.Date());
	
	String resv_date="";
	String resv_ready = "예약가능";
	String room_vip = resv_ready;
	String room_good = resv_ready;
	String room_bad = resv_ready;
	String[][] resv_arr = new String[30][5];
	
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// stmt 생성 및 연결 설정
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
			
	// ResultSet 선언 및 설정
//	ResultSet rset2 = stmt2.executeQuery("select * from joaresv where resv_date >= date_add(now(), interval+0 day) and resv_date < date_add(now(), interval+29 day);");
//	ResultSet rset3 = stmt3.executeQuery("select resv_date, ifnull(max(case when room=1 then name end),'예약가능') as 'room1', ifnull(max(case when room=2 then name end),'예약가능') as 'room2', ifnull(max(case when room=3 then name end),'예약가능') as 'room3' from joaresv where resv_date >= date_add(now(), interval+0 day) and resv_date < date_add(now(), interval+29 day) group by resv_date;");
	ResultSet rset3 = stmt3.executeQuery("select resv_date, ifnull(max(case when room=1 then name end),'예약가능') as 'room1', ifnull(max(case when room=2 then name end),'예약가능') as 'room2', ifnull(max(case when room=3 then name end),'예약가능') as 'room3' from joaresv where resv_date >= date_add('"+today+"', interval+0 day) and resv_date < date_add('"+today+"', interval+29 day) group by resv_date;");
%>
<%	
	for (int i=0; i<30; i++){
		resv_arr[i][0]=df.format(cal.getTime());
		resv_arr[i][1]=df2.format(cal.getTime());
		resv_arr[i][2]=room_vip;
		resv_arr[i][3]=room_good;
		resv_arr[i][4]=room_bad;
		cal.add(cal.DATE,+1);
	}
%>
<%	
	while(rset3.next()){
		resv_date = rset3.getString(1);
		room_vip = rset3.getString(2);
		room_good = rset3.getString(3);
		room_bad = rset3.getString(4);		
			
		for(int i=0; i<resv_arr.length; i++){
			if(resv_arr[i][0].equals(resv_date)){
				resv_arr[i][2] = rset3.getString(2);
				resv_arr[i][3] = rset3.getString(3);
				resv_arr[i][4] = rset3.getString(4);
			}
		}
	}
%>
<!-- 목록 출력용 테이블 생성 -->
<table cellspacing=1 border=1>
	<tr>
		<th>날짜</th>
		<th>vip</th>
		<th>일반</th>
		<th>합리</th>
	</tr>
<%
	for (int i=0; i<resv_arr.length; i++){
%>
		<tr>
		<td><%=resv_arr[i][0]%>(<%=resv_arr[i][1]%>)</td>
<%		
		if (resv_arr[i][2].equals("예약가능")){
%>
		<td><a href='d_02.jsp?resv_date=<%=resv_arr[i][0]%>&room=1'><%=resv_arr[i][2]%></a></td>
<%
		} else {
%>	
		<td><%=resv_arr[i][2]%></td>
<%		
		}
%>		
<%
		if (resv_arr[i][3].equals("예약가능")){
			out.println("<td><a href='d_02.jsp?resv_date="+resv_arr[i][0]+"&room=2'>"+resv_arr[i][3]+"</a></td>");			
		} else {
			out.println("<td>"+resv_arr[i][3]+"</td>");
		}
%>	
<%
		if (resv_arr[i][4].equals("예약가능")){
			out.println("<td><a href='d_02.jsp?resv_date="+resv_arr[i][0]+"&room=3'>"+resv_arr[i][4]+"</a></td>");			
		} else {
			out.println("<td>"+resv_arr[i][4]+"</td>");
		}
%>			
		</tr>
<%		
	}
%>
<%
	// 자원 반환
	rset3.close();
//	rset2.close();
	stmt2.close();
	stmt3.close();
	conn.close();
%>

</table>
</center>
</body>
</html>