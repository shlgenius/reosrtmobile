<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="java.sql.*,javax.sql.*,java.io.* ,java.text.* , java.util.*"%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="/bootstrap-4.2.1-dist/css/bootstrap.css">
<script src="/ckeditor/ckeditor.js"></script>
</head>
<style>
	td {
		padding-left: 15px;
	}
</style>
<script language="javascript">
	function submitForm(mode){
		fm.action = "write.jsp";
		fm.submit();
	}
</script>

<!-- include libraries(jQuery, bootstrap) -->
<!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">	-->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>	-->
<!-- <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>	-->

<!-- include summernote css/js -->
<!-- <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">	-->
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>	-->

<%
	request.setCharacterEncoding("UTF-8");
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.KOREA);
	df.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
	String today = df.format(new java.util.Date());
	
//	for(int i = 1; i<30; i++){
		cal.add(cal.DATE,+29);
//	}
	
	String lastDay = df.format(cal.getTime());
	String resv_date = request.getParameter("resv_date");
	String roomS = request.getParameter("room");
	if(roomS == null) {
		roomS = "0";
	}
%>

<body>
<center>
<h1>예약하기</h1><br>
<form method=post name="fm">

<div class="container text-center">
<!-- 테이블 출력 -->
<table width=800 cellspacing=1 border=1 class="table table-bordered">
	<tr>
		<td width=15%>성명</td>
		<td><input type=text class="form-control" name="name"></input></td>
	</tr>
	<tr>
		<td>예약일자</td>
		<td align=left>
			<input type=date name="resv_date" min=<%=today%> max=<%=lastDay%> value=<%=resv_date%>></input>
		</td>
	</tr>
	<tr>
		<td>예약방</td>
		<td align=left><select name="room">
<%
			if(roomS.equals("1")){
%>
				<option value=1 selected>VIP</option>
			<!--	<option value=2 >일반</option>  -->
			<!--	<option value=3 >합리</option>  -->
<%
			} else if(roomS.equals("2")){
%>
			<!--	<option value=1 >VIP</option>  -->
				<option value=2 selected>일반</option>
			<!--	<option value=3 >합리</option>  -->
<%				
			} else if(roomS.equals("3")){
%>
			<!--	<option value=1 >VIP</option> -->
			<!--	<option value=2 >일반</option>  -->
				<option value=3 selected>합리</option>
<%
			} else {
%>				<option>방선택</option>
				<option type=number value=1>VIP</option>
				<option type=number value=2>일반</option>
				<option type=number value=3>합리</option>
<%				
			}
%>
			</select>
		</td>
	</tr>
	<tr>
		<td>주소</td>
		<td><input type=text class="form-control" name="addr"></input></td>
	</tr>
	<tr>
		<td>전화번호</td>
		<td><input type=text class="form-control" name="tele"></input></td>
	</tr>
	<tr>
		<td>입금자명</td>
		<td><input type=text class="form-control" name="deposit_name"></input></td>
		
	</tr>
	<tr>
		<td>남기실말</td>
<!--	<td><textarea id="summernote" name="comment" cols=70 rows=300 class="textArea"></textarea>	-->
		<td style="padding-left:0px">
			<textarea id="editor1" name="content" cols=70 rows=300 class="textArea"></textarea>
			<script>
				var cont='';
				//ckeditor라는 오픈소스 에디터를 넣는다.
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                // CKEDITOR.replace( 'content' );
				var editor = CKEDITOR.replace( 'content', {hegiht: 300});

				// The "change" event is fired whenever a change is made in the editor.
				// var that = this;
				editor.on( 'change', function( evt ) {
					// getData() returns CKEditor's HTML content.
					cont = evt.editor.getData();
					console.log( 'Total bytes: ' + evt.editor.getData().length );
				});
            </script>
			<script>
				// $(document).ready(function() {
					// $('#summernote').summernote({
						// tabsize: 2,
						// height: 300,
					// });
				// });
			</script>		
		</td>
	</tr>
</div>
<br>

<div class="container text-center">	
<!-- 버튼용 테이블 생성 -->
<table width=800 cellspacing=1 border=0 class="table table-borderless">
	<tr >
		<td width=60%></td>		
		<td width=10% align=left></td>
		<td width=10% align=left></td>
		<td width=10% align=left></td>
		<td><input type="button" class="btn btn-outline-dark" value="예약하기" OnClick="submitForm('write')"></td>
	</tr>
</table>
</div>
</form>
</center>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="/bootstrap-4.2.1-dist/js/bootstrap.js"></script>
</body>
</html>