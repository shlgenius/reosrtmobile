<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>

<html>
<head>
</head>
<body>
<h1>create table gongji_table</h1>
<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	//stmt 생성 후 연결
	Statement stmt = conn.createStatement();
	
	// 예외 처리 
	try{		
		// 쿼리문 실행
		stmt.execute("create table joaresv (name varchar(20), resv_date date not null, room int not null, addr varchar(200), tele varchar(100), in_name varchar(20), comment text, write_date date, processing int, primary key (resv_date,room));");
		
		// 자원반환
		stmt.close();
		conn.close();
		
		out.println("OK");
		
	} catch(SQLException e){		
		// 예외 발생시 출력문
		out.println("테이블 생성 실패 ");
	}
%>
</body>
</html>