<!-- html과 jsp에서 한글처리 지시 -->
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<!-- 필요한 부분 import -->
<%@ page import="java.sql.*,javax.sql.*,java.io.*,java.text.*, java.util.*"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="user-scable= no, width=device-width" />
<link rel="stylesheet" href="/bootstrap-4.2.1-dist/css/bootstrap.css">

<!-- css 스타일 선언 : a 링크 스타일 -->
<style></style>
</head>
<body>
<center>
<h1>예약상황</h1> <br>
<%
	// 달력 활용을 위한 설정
	Calendar cal = Calendar.getInstance();
	
	// 한국 시간을 가지고 오기 위한 설정
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.KOREA);
	df.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
	
	// 한국 시간의 요일을 가지고 오기 위한 설정
	SimpleDateFormat df2 = new SimpleDateFormat("EEE",Locale.KOREA);
	df2.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
	
	String today = df.format(new java.util.Date());//현재 날짜
	
//	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd",Locale.KOREA);
//	formatter.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
//	String today = formatter.format(new java.util.Date());
	
	// 결과값을 저장하기 위한 변수 선언
	String resv_date="";
	String resv_ready = "예약가능";
	String room_vip = resv_ready;
	String room_good = resv_ready;
	String room_bad = resv_ready;
	String[][] resv_arr = new String[30][5];	// 결과값을 배열에 저장하기 위한 배열 선언
	
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// stmt 생성 및 연결 설정
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
			
	// ResultSet 선언 및 설정
//	ResultSet rset2 = stmt2.executeQuery("select * from joaresv where resv_date >= date_add(now(), interval+0 day) and resv_date < date_add(now(), interval+29 day);");
//	ResultSet rset3 = stmt3.executeQuery("select resv_date, ifnull(max(case when room=1 then name end),'예약가능') as 'room1', ifnull(max(case when room=2 then name end),'예약가능') as 'room2', ifnull(max(case when room=3 then name end),'예약가능') as 'room3' from joaresv where resv_date >= date_add(now(), interval+0 day) and resv_date < date_add(now(), interval+29 day) group by resv_date;");
	ResultSet rset3 = stmt3.executeQuery("select resv_date, ifnull(max(case when room=1 then name end),'예약가능') as 'room1', ifnull(max(case when room=2 then name end),'예약가능') as 'room2', ifnull(max(case when room=3 then name end),'예약가능') as 'room3' from joaresv where resv_date >= date_add('"+today+"', interval+0 day) and resv_date < date_add('"+today+"', interval+29 day) group by resv_date;");
%>
<%	
	// 반복문을 활용하여 배열에 지정한 값을 저장
	for (int i=0; i<30; i++){
		resv_arr[i][0]=df.format(cal.getTime());
		resv_arr[i][1]=df2.format(cal.getTime());
		resv_arr[i][2]=room_vip;
		resv_arr[i][3]=room_good;
		resv_arr[i][4]=room_bad;
		cal.add(cal.DATE,+1);	// 반복문을 끝내기 전에 날짜에 하루씩 더하여 준다
	}
%>
<%	
	// 결과값이 존재하는 동안 반복문 실행
	while(rset3.next()){
		resv_date = rset3.getString(1);		// 날짜
		room_vip = rset3.getString(2);		// VIP룸 
		room_good = rset3.getString(3);		// 일반룸
		room_bad = rset3.getString(4);		// 합리룸
		
		// 반복문을 배열의 길이만큼 실행
		for(int i=0; i<resv_arr.length; i++){
			
			// 결과값으로 받은 날짜와 기존에 미리 지정한 날짜가 일치할 경우 이하 조건문 실행
			if(resv_arr[i][0].equals(resv_date)){		
				resv_arr[i][2] = rset3.getString(2);	// 해당 배열에 VIP 값 저장
				resv_arr[i][3] = rset3.getString(3);	// 해당 배열에 일반 값 저장
				resv_arr[i][4] = rset3.getString(4);	// 해당 배열에 합리 값 저장
			}
		}
	}
%>
<div class="container text-center">
<!-- 목록 출력용 테이블 생성 -->
<div class="table-responsive-md">
<table width=1000 class="table">
	<thead class="thead-dark">
	<tr align=center>
		<th scope="col">날짜</th>
		<th scope="col">VIP</th>
		<th scope="col">일반</th>
		<th scope="col">합리</tH>
	</tr>
	</thead>
	<tbody>
<%
	for (int i=0; i<resv_arr.length; i++){
%>
		<tr align=center>
<%
		if(resv_arr[i][1].equals("토")){			// 토요일일 경우 색깔 지정
			out.println("<td><font color='#2E2EFE'>"+resv_arr[i][0]+"("+resv_arr[i][1]+")</font></td>");
		} else if(resv_arr[i][1].equals("일")){	// 일요일일 경우	색깔 지정 
			out.println("<td><font color='#FE2E2E'>"+resv_arr[i][0]+"("+resv_arr[i][1]+")</font></td>");
		} else {								// 평일일 경우 색깔 지정
			out.println("<td>"+resv_arr[i][0]+"("+resv_arr[i][1]+")</td>");
		}

		if (resv_arr[i][2].equals("예약가능")){		// 예약가능일 경우 링크 설정
%>
			<td><a href='d_02.jsp?resv_date=<%=resv_arr[i][0]%>&room=1'><%=resv_arr[i][2]%></a></td>
<%
		} else {
%>	
			<td><%=resv_arr[i][2]%></td>
<%		
		}
%>		
<%
		if (resv_arr[i][3].equals("예약가능")){		// 예약가능일 경우 링크 설정
			out.println("<td><a href='d_02.jsp?resv_date="+resv_arr[i][0]+"&room=2'>"+resv_arr[i][3]+"</a></td>");			
		} else {
			out.println("<td>"+resv_arr[i][3]+"</td>");
		}
%>	
<%
		if (resv_arr[i][4].equals("예약가능")){		// 예약가능일 경우 링크 설정
			out.println("<td><a href='d_02.jsp?resv_date="+resv_arr[i][0]+"&room=3'>"+resv_arr[i][4]+"</a></td>");			
		} else {
			out.println("<td>"+resv_arr[i][4]+"</td>");
		}	
	}	
%>
	</tr>
<%
	// 자원 반환
	rset3.close();
//	rset2.close();
	stmt2.close();
	stmt3.close();
	conn.close();
%>
	</tbody>
</table>
	</div>
</div>
</center>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="/bootstrap-4.2.1-dist/js/bootstrap.js"></script>
</body>
</html>